# API-exercise

This is my solution to the Container Solutions API Exercise. Here you can see the usage instructions and some examples of how to use the API. More information can be found at the [Documentation](docs/DOCUMENTATION.md) 

## Usage
* Install and run Minikube (you can check how to do this [here](https://minikube.sigs.k8s.io/docs/start/))
* Clone the project repository
* Run the `deploy.sh` script
* Access the API on the given IP

## Examples

All the following examples will be done using Curl but you can use another tool you prefer (like a browser, for example)

### List all the people on database

```
$ curl http://192.168.99.106:30000/people | jq | more
{
  "People": [
    {
      "age": 22,
      "fare": 7.25,
      "name": "Mr. Owen Harris Braund",
      "parentsOrChildrenAboard": 0,
      "passengerClass": 3,
      "sex": "male",
      "siblingsOrSpousesAboard": 1,
      "survived": false,
      "uuid": 1
    },
    {
      "age": 38,
      "fare": 71.2833,
      "name": "Mrs. John Bradley (Florence Briggs Thayer) Cumings",
      "parentsOrChildrenAboard": 0,
      "passengerClass": 1,
      "sex": "female",
...
...
...
```

### See information on one person
```
$ curl http://192.168.99.106:30000/people/1
{"Person":{"age":22.0,"fare":7.25,"name":"Mr. Owen Harris Braund","parentsOrChildrenAboard":0,"passengerClass":3,"sex":"male","siblingsOrSpousesAboard":1,"survived":false,"uuid":1},"message":"success"}
```
### Add person to the database 
```
$ curl -X POST -H "Content-Type: application/json" -d '{"name": "Mr. Test Tests", "age": "99", "sex": "other", "survived": "True", "siblingsOrSpousesAboard": "2", "parentsOrChildrenAboard": "2", "passengerClass": "1", "fare":"1.23"  }' http://192.168.99.106:30000/people
{"message":"Person Mr. Test Tests has been created successfully."}
```
### Delete person from the database
```
$ curl -X DELETE http://192.168.99.106:30000/people/888
{"message":"Person Mr. Test Tests successfully deleted."}
```