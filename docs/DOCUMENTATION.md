# Documentation

This is the documentation for the API Exercise. Here we will show the project design, and discuss the solutions used on each step of it. 


## Solution design

I followed a typical three-tier model to design this solution, to make it closer to a real implementation. As shown on the figure below, I used an Nginx deployment as a reverse proxy in front of the API server application, another Deployment to host the API server and a StatefulSet for the PostgreSQL database. The Deployments are the best option here as they ensure easy scalability. 

![Solution Design](design.png)

## Database
To make this project closer to a real one, and to have access to more community support, I opted to use PostgreSQL. I changed the CSV file replacing the zeros and ones on the boolean column 'survived' with 'True' and 'False'. I did that to make it easier to import the table using a single COPY command. As it does not change the structure of the table or the values of the fields on this column, I took the liberty to do that. 

## API Server
I used Flask to create the API server application, because I found it easier to use for this purpose. The migration scripts use Flask-Migrate. I changed the model on the field 'age', because on the data, there are some cases where the field value has a decimal point, like on row 58, for example. On this case, I found it better to change the model to fit the data than the opposite. 

## Docker
On the Dockerfiles i try to follow the instructions given along with the exercise where possible, using a multistage Dockerfile to build the required libs for the API server application, for example. As I use Kubernetes for the deploy, some configuration options were set there instead on the Dockerfiles (such as environment variables).

## Kubernetes
This Kubernetes implementation is also very straightforward, using an StatefulSet for the database, and Deployments for the application and reverse proxy. I use an Secret resource to store the database password. Unfortunately, I did not found a practical way to orchestrate deployment of the application container after the database one, as it take a very long time to deploy, so I was forced to do that on the deploy script. 

## Deploy script

The script used to deploy does not have any complex logic. It just builds the Dockerfiles using minikube Docker, starts the Kubernetes resources, waiting for the database container to be up and ready before deployment of the other containers, and finally imports the data on the CSV to the database server after the application container runs the migration script to create the table. 
