#!/bin/bash
# Deploy script for Container Solutions API Exercise 
# Author: Joao Paulo Lourenço <jplourenco@gmail.com>

echo "API Exercise Deploy Script"
echo "Please make sure that Minikube is installed and running on your system"
read -p "Enter the Database password: " db_password

eval $(minikube docker-env)

docker build -t apiserver/apiserver:v1 apiserver/.
docker build -t apiserver/nginx:v1 nginx/.

kubectl create secret generic db-secret --from-literal=password=$db_password 

kubectl apply -f kubernetes/postgres.yaml 
kubectl apply -f kubernetes/postgres-service.yaml 

until kubectl exec --stdin --tty postgres-database-0 -- psql -U apiserver -d apiserver -c '\dt'
do 
    sleep 1
done

kubectl apply -f kubernetes/apiserver.yaml
kubectl apply -f kubernetes/apiserver-service.yaml 
kubectl apply -f kubernetes/nginx.yaml 
kubectl apply -f kubernetes/nginx-service.yaml 

until kubectl exec --stdin --tty postgres-database-0 -- psql -U apiserver -d apiserver -c '\d people'
do 
    sleep 1
done

kubectl cp titanic.csv postgres-database-0:/tmp/

kubectl exec --stdin --tty postgres-database-0 -- psql -U apiserver -d apiserver -c \
"COPY people ("survived",\"passengerClass\","name","sex","age",\"siblingsOrSpousesAboard\",\"parentsOrChildrenAboard\","fare") \
FROM '/tmp/titanic.csv' DELIMITER ',' CSV HEADER;"

echo "Deploy done. You can access the API at http://$(minikube ip):30000"