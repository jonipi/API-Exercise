import os
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
URI_TEMPLATE = "postgresql://{user}:{passwd}@{host}:5432/apiserver"
db_user = os.getenv("DATABASE_USER")
db_pass = os.getenv("DATABASE_PASS")
db_host = os.getenv("DATABASE_HOST")

app.config["SQLALCHEMY_DATABASE_URI"] = URI_TEMPLATE.format(
    user=db_user, passwd=db_pass, host=db_host
)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)

class PeopleModel(db.Model):
    __tablename__ = "people"

    survived = db.Column(db.Boolean())
    passengerClass = db.Column(db.Integer())
    name = db.Column(db.String())
    sex = db.Column(db.String())
    age = db.Column(db.Numeric())
    siblingsOrSpousesAboard = db.Column(db.Integer())
    parentsOrChildrenAboard = db.Column(db.Integer())
    fare = db.Column(db.Numeric())
    uuid = db.Column(db.Integer, primary_key=True)

    def __init__(
        self,
        name,
        sex,
        age,
        passengerClass,
        survived,
        siblingsOrSpousesAboard,
        parentsOrChildrenAboard,
        fare,
    ):
        self.survived = survived
        self.passengerClass = passengerClass
        self.name = name
        self.sex = sex
        self.age = age
        self.siblingsOrSpousesAboard = siblingsOrSpousesAboard
        self.parentsOrChildrenAboard = parentsOrChildrenAboard
        self.fare = fare

    def __repr__(self):
        return f"<Person {self.name}>"

@app.route("/")
def hello():
    return {"People": "Titanic"}

@app.route("/people", methods=["POST"])
def set_people():
    if request.is_json:
        data = request.get_json()
        new_person = PeopleModel(
            name=data["name"],
            age=data["age"],
            sex=data["sex"],
            survived=bool(data["survived"]),
            passengerClass=data["passengerClass"],
            siblingsOrSpousesAboard=data["siblingsOrSpousesAboard"],
            parentsOrChildrenAboard=data["parentsOrChildrenAboard"],
            fare=data["fare"],
        )

        db.session.add(new_person)
        db.session.commit()

        return {
            "message": f"Person {new_person.name} has been created successfully."
        }
    else:
        return {"error": "The request payload is not in JSON format"}

@app.route("/people", methods=["GET"])
def get_people():
    people = PeopleModel.query.all()
    results = [
        {
            "name": person.name,
            "sex": person.sex,
            "fare": float(person.fare),
            "survived": person.survived,
            "uuid": person.uuid,
            "siblingsOrSpousesAboard": person.siblingsOrSpousesAboard,
            "parentsOrChildrenAboard": person.parentsOrChildrenAboard,
            "passengerClass": person.passengerClass,
            "age": float(person.age),
        }
        for person in people 
    ]

    return {"count": len(results), "People": results, "message": "success"}

@app.route('/people/<person_id>', methods=['GET'])
def get_person(person_id):
    person = PeopleModel.query.get_or_404(person_id)

    response = {
        "name": person.name,
        "sex": person.sex,
        "fare": float(person.fare),
        "survived": person.survived,
        "uuid": person.uuid,
        "siblingsOrSpousesAboard": person.siblingsOrSpousesAboard,
        "parentsOrChildrenAboard": person.parentsOrChildrenAboard,
        "passengerClass": person.passengerClass,
        "age": float(person.age)
    }
    return {"message": "success", "Person": response}  
  
@app.route('/people/<person_id>', methods=['PUT'])
def set_person(person_id):
    person = PeopleModel.query.get_or_404(person_id)

    data = request.get_json()
    person.name = data['name'] 
    person.age = data['age']
    person.sex = data['sex']
    person.survived = bool(data['survived'])
    person.passengerClass = data['passengerClass']
    person.siblingsOrSpousesAboard = data['siblingsOrSpousesAboard']
    person.parentsOrChildrenAboard = data['parentsOrChildrenAboard']
    person.fare = data['fare']

    db.session.add(person)
    db.session.commit()
        
    return {"message": f"Person {person.name} successfully updated"}

@app.route('/people/<person_id>', methods=['DELETE'])
def delete_person(person_id):
    person = PeopleModel.query.get_or_404(person_id)

    db.session.delete(person)
    db.session.commit()
        
    return {"message": f"Person {person.name} successfully deleted."}

if __name__ == "__main__":
    app.run(debug=True)
