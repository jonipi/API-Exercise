"""empty message

Revision ID: c496908492a7
Revises: 
Create Date: 2021-02-10 08:19:28.562795

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c496908492a7'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('people',
    sa.Column('survived', sa.Boolean(), nullable=True),
    sa.Column('passengerClass', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('sex', sa.String(), nullable=True),
    sa.Column('age', sa.Numeric(), nullable=True),
    sa.Column('siblingsOrSpousesAboard', sa.Integer(), nullable=True),
    sa.Column('parentsOrChildrenAboard', sa.Integer(), nullable=True),
    sa.Column('fare', sa.Numeric(), nullable=True),
    sa.Column('uuid', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('uuid')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('people')
    # ### end Alembic commands ###
